// fast, even though bruteforce

#include "../utils/defs.h"

int main()
{
    int sum = 0;
    int sumOfSquares = 0;
    int squareOfSum = 0;

    for (int i = 1; i <= 100; i++)
    {
        sum+=i;
        sumOfSquares+=i*i;
    }

    cout << sum*sum - sumOfSquares << endl;
}