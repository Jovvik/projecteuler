// 4.692s vs 5.9s previously

#include "../utils/defs.h"
#include "../utils/primes.cpp"

#include <algorithm>
#include <set>

int main()
{
    ulli input = 5e7;
    vector<ulli> primes = sieveOfEratosthenes(sqrt(input));

    ulli cnt = 0;
    
    set<int> s;
    vector<ulli> primes2; // power of 2
    vector<ulli> primes3; // power of 3
    vector<ulli> primes4; // power of 4

    ulli t;
    for (ulli i = 0; i < primes.size(); i++)
    {
        t = pow(primes[i], 2);
        primes2.push_back(t);
        t *= primes[i];
        primes3.push_back(t);
        t *= primes[i];
        primes4.push_back(t);
    }
    for (ulli i = 0; i < primes.size(); i++)
    {
        int sum1 = primes4[i];
        if (sum1 >= input)
            break;
        for (ulli j = 0; j < primes.size(); j++)
        {
            int sum2 = sum1 + primes3[j];
            if (sum2 >= input)
                break;
            for (ulli k = 0; k < primes.size(); k++)
            {
                int sum3 = sum2 + primes2[k];
                if (sum3 > input)
                    break;
                cnt++;
                s.insert(sum3);
            }
        }
    }
    cout << s.size() << endl;
    return 0;
}