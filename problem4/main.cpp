// fast (46msecs)

// it's safe to assume the product is 6-digit
// a*100000 + b * 10000 + c * 1000 + c * 100 + b * 10 + a
// 100001a + 10010b + 1100c = f*e

#include "../utils/defs.h"
#include "../utils/palindrome.cpp"

int main()
{
    int res = 0;
    int product;

    for (short a = 1; a <= 9; a++)
    {
        for (short b = 0; b <= 9; b++)
        {
            for (short c = 0; c <= 9; c++)
            {
                product = 100001 * a + 10010 * b + 1100 * c;
                for (int f = 100; f <= 999; f++)
                {
                    if (product % f == 0)
                    {
                        if (product / f <= 999 && product > res)
                        {
                            res = product;
                        }
                    }
                }
            }
        }
    }
    cout << res;
    return 0;
}