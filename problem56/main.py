# python OP as always, bruteforce in 0.146s

def sumOfDigits(n):
    res = 0
    while n > 0:
        res += n % 10
        n //= 10
    return res

maxval = 0
for a in range(100):
    for b in range(100):
        t = sumOfDigits(a ** b)
        if t > maxval:
            maxval = t
            print(maxval, a, b)