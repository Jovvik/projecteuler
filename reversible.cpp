// Solution for #145

#include "iostream"

bool reversible(int n)
{
    if (n % 10 == 0)
        return false;

    int nn = n;
    int reversed = 0;

    while (nn > 0)
    {
        int digit = nn % 10;
        nn /= 10;
        reversed *= 10;
        reversed += digit;
    }

    int sum = reversed + n;

    while (sum > 0)
    {
        int digit = sum % 10;
        sum /= 10;
        if (digit % 2 == 0)
            return false;
    }
    
    return true;
}

int main(int argc, const char* argv[])
{
    int cnt = 0;
    for (int i = 1; i < atoi(argv[1]); i++)
    {
        if (reversible(i))
            cnt++;
    }
    std::cout << cnt << std::endl;
}