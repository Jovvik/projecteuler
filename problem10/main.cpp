// fast-ish (0.117s)

#include "../utils/defs.h"
#include "../utils/primes.cpp"

int main()
{
    ulli res = 0;
    vector<ulli> primes = sieveOfEratosthenes(2000000);
    for (ulli prime : primes)
    {
        // cout << prime << endl;
        res+=prime;
    }
    cout << res << endl;
}