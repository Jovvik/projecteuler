#!/bin/bash

# shell script that compiles and runs problem from args
# it assumes you run it from root folder of repo

# language-specific commands to compile and run
declare -A commands=( ['c++']="g++ -o main.bin main.cpp && time ./main.bin" ['c++fast']="g++ -o main_fast.bin -Ofast main.cpp && time ./main_fast.bin" ['python']='time python3 main.py' )

if [ "$1" == "" ]; then
    echo "No problem is specified, aborting..."
    exit
else
    problem_id=$1
fi

cd "problem$problem_id"

lang="c++"
if [ "$2" == "" ]; then
    echo "No language is specified, defaulting to $lang"
else
    lang=$2
fi

# escaped with /t to prevent false positives for substrings
if [[ "${commands[$lang]}" == "" ]]; then
    echo "The language is not supported yet"
    exit
fi

eval ${commands[$lang]}