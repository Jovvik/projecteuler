// fast, even though it's bruteforce

#include "../utils/defs.h"
#include "../utils/primes.cpp"

int main()
{
    ulli input_num = 600851475143;
    ulli res = 0;
    vector<ulli> primes = sieveOfEratosthenes(sqrt(input_num));
    for (int prime : primes)
    {
        if (input_num % prime == 0)
            res = prime;
    }
    cout << res << endl;
    return 0;
}