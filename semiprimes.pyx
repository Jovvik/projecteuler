from math import sqrt

cdef int semiprime(int n):
    cdef int i, cnt=0
    for i in range(2, int(sqrt(n) + 1)):
        if not n % i:
            if n % (i**3) == 0:
                return 0
            cnt+=2
            if cnt > 2:
                return 0
    if cnt == 2:
        return 1
    return 0

cpdef int solve(int n):
    cdef int res = 0, i
    for i in range(2, n):
        if semiprime(i):
            res+=1
        if i % 1000000 == 0:
            print(i)
    return res

cpdef list solveprint(int n):
    cdef int i
    cdef list ls = []
    for i in range(2, n):
        if semiprime(i):
            ls.append(i)
    return ls