def solve(int n):
    cdef:
        int x, y, z, s
        list ls = []
    for x in range(1, n):
        y = x + 1
        z = y + 1
        while x + y + z <= n:
            s = x + y + z
            if s > n:
                break
            while z*z < x*x + y*y:
                z += 1
            if z*z == x*x + y*y and s <= n:
                ls.append(s)
            y += 1
    a = set(ls)
    return a