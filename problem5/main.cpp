// fast

#include "../utils/defs.h"
#include "../utils/gcd.h"
#include <vector>

int main()
{
    int res = 1;
    bool flag = false;
    for (int i = 2; i <= 20; i++)
    {
        res = lcm(res, i);
    }
    cout << res << endl;
    return 0;
}