// not solved yet

#include "../utils/defs.h"

int main()
{
    int rmax = 0;
    for (int a = 3; a < 1001; a++)
    {
        for (int n = 1; n < 10; n++)
        {
            int r = ((int)pow(a-1, n) + (int)pow(a+1, n)) % (a*a);
            if (r > rmax)
                rmax = r;
        }
    }
    cout << rmax << endl;
}