// # 125

#include "iostream"
#include "vector"

// Checks if a number is palindromic
// Reverses the number and checks equality
bool palindromic(int n)
{
    int nn = n;
    int reversed = 0;

    while (nn > 0)
    {
        int digit = nn % 10;
        nn /= 10;
        reversed *= 10;
        reversed += digit;
    }

    if (reversed == n)
        return true;
    return false;
}

// Finds sum of squares of consecutive numbers
// Numbers from "first" and of "length"
int sums(int first, int length)
{
    int sum = 0;
    for (int i = 0; i < length; i++)
    {
        sum += (first + i) * (first + i);
    }
    return sum;
}

int main(int argc, const char* argv[])
{
    int64_t result = 0;
    for (int i = 1; i < 10000; i++)
    {
        for (int j = 2; true; j++) 
        {
            int num = sums(i, j);
            if (num >= 100000000)
                break;
            if (palindromic(num))
            {
                result += num;
            }
        }
    }
    std::cout << result << std::endl;
}