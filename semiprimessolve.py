from pyximport import install
install()

from semiprimes import solve

from sys import argv

print(solve(int(argv[1])))