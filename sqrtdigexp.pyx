from decimal import *

cdef int digitsum(int n):
    cdef:
        int s = 0
        Decimal m = Decimal(2).sqrt()
    if m % 1 == 0:
        return 0
    for _ in range(100):
        m%=1
        m*=10
        s+=int(m%10)
    return s

cpdef int solve(int length):
    cdef s = 0, i
    for i in range(2, length):
        s += digitsum(i)
    return s