// fast

#include "../utils/defs.h"

bool bouncy(int n)
{
    if (n < 100)
        return false;
    
    int prevd = n % 10;
    bool decreasing = false;
    bool increasing = false;
    int d;
    while (n > 0)
    {
        d = n % 10;
        n /= 10;
        if (prevd > d)
            increasing = true;
        else if (prevd < d)
            decreasing = true;
        
        if (increasing && decreasing)
            return true;
        prevd = d;
    }
    return false;
}

int main()
{
    int cnt = 0;
    float prop = 0;
    int i = 99;
    while (cnt != 0.99 * i)
    {
        i++;
        if (bouncy(i))
            cnt++;
        // cout << float(cnt) / i << endl;
    }
    cout << i << endl;
}