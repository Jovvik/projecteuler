# slow cuz python (3.463)

def bouncy(n):
    if n < 100:
        return False
    pd = n % 10
    decreasing = False
    increasing = False
    while n > 0:
        d = n % 10
        n //= 10
        if pd > d:
            increasing = True
        if pd < d:
            decreasing = True
        if increasing and decreasing:
            return True
        pd = d
    return False

count = 0
for i in range(100, 5000000):
    if bouncy(i):
        count+=1
    if count / i == 0.99:
        print(i)
        exit()