#include <math.h>
#include <vector>

using namespace std;

// all factors of n
vector<int> factors(int n)
{
    vector<int> res;
    int t = sqrt(n);
    for (int i = 1; i < t; i++)
    {
        if (n % i == 0)
        {
            if (float(n) / i == i)
                res.push_back(i);
            else
            {
                res.push_back(i);
                res.push_back(n / i);
            }
        }
    }
    return res;
}

// amount of factors of n
int factorCnt(int n)
{
    return factors(n).size();
}