#include "defs.h"
#include <cstring>
#include <vector>

// returns vector of all primes under *n*
// TODO: rewrite return type for array instead of vector
// benchmarks: 
// 100 times n=1e6 : 1.077s
// 1 time n=1e8 : 1.671s
// n=1e9 : 18.2s
vector<ulli> sieveOfEratosthenes(ulli n)
{
    bool* prime = new bool[n+1];
    memset(prime, true, sizeof(bool[n+1])); 
  
    for (ulli p=2; p*p<=n; p++) 
    { 
        // If prime[p] is not changed, then it is a prime 
        if (prime[p] == true) 
        { 
            // Update all multiples of p 
            for (ulli i=p*2; i<=n; i += p) 
                prime[i] = false; 
        } 
    }

    vector<ulli> res;
    for (ulli i = 2; i<=n; i++)
        if (prime[i])
            res.push_back(i);
    
    delete[] prime;
    return res;
}