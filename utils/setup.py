from distutils.core import setup
from Cython.Build import cythonize
from sys import argv

setup(ext_modules=cythonize('singular_integer_right_trinagles.pyx'))