#include "defs.h"
#include <cmath> 
#include <math.h>
#include <vector>

//! old version, doesn't work properly with zeros in mid of number
// checks if *n* is a palindrome
// benchmarks: (1 to 1e7) - 1.017s
bool isPalindrome_(int n)
{
    while (n!=0)
    {
        int pow10 = pow(10, floor(log10(n)));
        // cout << n << " " << pow10 << " " << n / pow10 << " " << n % 10 << endl;
        if (n / pow10 != n % 10)
            return false;
        n %= pow10;
        n /= 10;
    }
    return true;
}

// checks if *n* is a palindrome
// benchmarks: (1 to 1e7) - 9.305s
// (1 to 1e8) - 1m34.286s
bool isPalindrome(int n)
{
    vector<short> s;
    while(n!=0)
    {
        s.push_back(n%10);
        n/=10;
    }

    for (lli i = 0; i < s.size() / 2; i++)
    {
        if (s[i] != s[s.size() - 1 - i])
            return false;
    }
    return true;
}