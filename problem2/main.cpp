// even Fibonacci numbers, problem #2
// fast, even though it's bruteforce

#include "../utils/defs.h"

int main()
{
    ulli prev2=0, prev=1, curr=0;
    ulli sum = 0;
    while(prev+prev2 < 4e6) //less than 4 million
    {
        curr = prev+prev2;

        prev2 = prev;
        prev = curr;

        if (curr % 2 == 0)
            sum+=curr;

    }
    cout << sum << endl;
}