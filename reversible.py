# Pseudo-code for problem #145
# Very slow

def reversible(n):
    if n % 10 == 0:
        return False
    nn = n
    rev = 0
    while nn > 0:
        d = nn % 10
        nn //= 10
        rev *= 10
        rev += d
    a = rev + n
    while a > 0:
        d = a % 10
        a //= 10
        if d % 2 == 0:
            return False
    return True

cnt = 0
for i in range(int(10e9)):
    if i % 10e6 == 0:
        print(i // 10e6)
    if reversible(i):
        cnt+=1
print(cnt)