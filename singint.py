from pyximport import install
install()

from singular_integer_right_trinagles import solve
from sys import argv

print(sum(solve(int(argv[1]))))