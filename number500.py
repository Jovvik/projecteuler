from utils.primes import prime

res = 1
primes = set([2])
i, p = 2, 0
while True:
    if prime(i, primes):
        p += 1
        res *= i
        res %= 500500507
        if p == 500500:
            print(res)
            exit()
    i += 1