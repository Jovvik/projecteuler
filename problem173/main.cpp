// fast

#include "../utils/defs.h"

int main()
{
    int input = 1e6;
    int res = 0;
    for (int i = 3; ; i++)
    {
        int sum = 0;
        for (int j = i; j>=3; j-=2)
        {
            int length = 4*(j-1);
            if (sum + length > input)
                break;

            sum += length;
            res++;
        }
        if (sum == 0)
            break;
    }
    cout << res << endl;
    return 0;
}