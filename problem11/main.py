# fast
ints = []

with open('grid.txt', 'r') as f:
    s = f.readlines()
    for line in s:
        ints.append([int(x) for x in line.split()])

print(ints)

res = 0

for x in range(20):
    for y in range(20):
        vert = 1
        horiz = 1
        diag1 = 1
        diag2 = 1
        for z in range(4):
            if y+z>=20:
                vert = 0
            else:
                vert *= ints[x][y+z]
            if x+z>=20:
                horiz = 0
            else:
                horiz *= ints[x+z][y]
            if x+z>=20 or y-z<0:
                diag1 = 0
            else:
                diag1 *= ints[x+z][y-z]
            if x+z>=20 or y+z>=20:
                diag2 = 0
            else:
                diag2 *= ints[x+z][y+z]
        
        for direction in [vert, horiz, diag1, diag2]:
            if direction > res:
                res = direction
                print(x, y, direction)

print(res)