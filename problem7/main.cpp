// fast

#include "../utils/defs.h"
#include "../utils/primes.cpp"

int main()
{
    vector<ulli> primes = sieveOfEratosthenes(1000000);
    cout << primes[10000] << endl;
}