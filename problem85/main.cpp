// Problem #85, solved
// techincally heuristic, slow

#include "iostream"

int64_t rects(int x, int y)
{
    int64_t sum = 0;
    
    for(int xr = 0; xr < x; xr++)
    {   
        for(int yr = 0; yr < y; yr++)
        {
            sum += (x - xr) * (y - yr);
        }
    }
    return sum;
}

int main(int argc, char const *argv[])
{
    int mindiff = INT32_MAX;
    for (int x = 1; x < 1000; x++)
    {
        for (int y = x+1; y < 1000; y++)
        {
            int diff = rects(x, y) - 2000000;
            if (diff > mindiff)
                break;
            diff = abs(diff);
            if (diff < mindiff)
            {
                std::cout << "New closest found @ " << diff << " diff, " << " x: " << x << " y: " << y << " area: " << x*y << std::endl;
                mindiff = diff; 
            }
        }
    }
    return 0;
}