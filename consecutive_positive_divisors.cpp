#include "iostream"
#include "vector"

int divisors(int n)
{
    int count = 2; //itself, 1
    for (int i = 2; i < n / 2 + 1; i++)
    {
        if (n % i == 0)
        {
            count++;
        }
    }
    return count;
}

int main(int argc, const char* argv[])
{
    std::vector<int> divs;
    time_t start, prom;
    time(&start);
    for (size_t i = 1; i < 10000000; i++)
    {
        divs.push_back(divisors(i));
        if (i % 100000 == 0)
        {
            time(&prom);
            std::cout << difftime(prom, start) << std::endl;
        }
    }

    std::cout << "Calc done" << std::endl;

    int count = 0;
    for (size_t i = 0; i < divs.size(); i++)
    {
        if (divs[i] == divs[i+1])
        {
            count++;
        }
    }

    std::cout << count << std::endl;
}