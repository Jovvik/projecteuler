// fast

#include "../utils/defs.h"
#include <vector>

pair<ulli, int> collatzSequenceLength(ulli n, vector<bool>& knownLengths)
{
    ulli cnt = 1;
    while (n != 1)
    {
        if (n <= 1e6)
            if (knownLengths[n])
                return pair<ulli, int>(cnt, n);
        if (n % 2 == 0)
            n /= 2;
        else
            n = n * 3 + 1;
        cnt++;
    }

    return pair<ulli, int>(cnt, -1);
}

int main()
{
    vector<bool> knownLens(int(1e6), false);
    vector<int> lens(int(1e6), 0);
    ulli maxLen = 0;
    ulli len;
    int bestNum;
    for (int i = 1e5; i < 1e6; i++)
    {
        auto smth = collatzSequenceLength(i, knownLens);
        len = smth.first;
        if (smth.second != -1)
            len+=lens[smth.second];
        knownLens[i] = true;
        lens[i] = len;
        if (len > maxLen)
        {
            maxLen = len;
            bestNum = i;
            cout << bestNum << " " << maxLen << endl;
        }
    }
    cout << bestNum << " " << maxLen << endl;
    return 0;
}