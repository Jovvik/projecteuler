// fast, 0.168s

#include "../utils/defs.h"
#include "../utils/factors.h"

int main()
{
    int factorNum = 0;
    int triangleNum = 0;
    int naturalNum = 0;
    while (factorNum <= 500)
    {
        naturalNum++;
        triangleNum += naturalNum;
        factorNum = factorCnt(triangleNum);
    }
    cout << triangleNum << endl;
    return 0;
}