//46 msecs, really good

#include "../utils/defs.h"
#include "../utils/primes.cpp"

#include <algorithm>

struct s
{
    int n;
    int rad;
};

const bool operator<(s s1, s s2)
{
    if (s1.rad == s2.rad)
        return s1.n < s2.n;
    return s1.rad < s2.rad;
}

int main()
{
    vector<s> v(100000);
    for (int n = 1; n <= 100000;)
    {
        v[n].n = n;
        v[n].rad = 1;
        n++;
        v[n].n = n;
        v[n].rad = 2;
        n++;
    }

    for (int n = 3; n <= 100000; n+=2)
    {
        if (v[n].rad == 1)
        {
            for (int i = n; i <= 100000; i+=n)
            {
                v[i].rad*=n;
            }
        }
    }

    sort(v.begin(), v.end());
    cout << v[10000-1].n << endl;
}