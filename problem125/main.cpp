// fast

#include <set>

#include "../utils/defs.h"
#include "../utils/palindrome.cpp"

int main(int argc, char * argv[])
{
    int input = 1e8;
    set<int> lv_Table;
  
    int squares[7072] ;

    for (int i = 1; i < 7072; i++)
    {
        squares[i] = i * i;
    }

    ulli res = 0;
    for (int i = 7071; i > 0; i--)
    {
        int t = 0 ;
        
        t = squares[i];
        for (int j = i - 1; j > 0; j--)
        {
            t += squares[j];
            
            if (t < 100000000)
            {
                if (isPalindrome(t) && lv_Table.insert(t).second)
                {
                    res += t; 
                }
            }
            else
                break;
        }
    }
  
    cout << res << endl;

	return 0;
}