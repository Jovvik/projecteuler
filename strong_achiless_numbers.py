import math

# Check if a is divisor of b
def divisor(a, b):
	return not (b % a)
## Return list of all primes under n
def primes(n):
    if n<=2:
        return []
    sieve=[True]*(n+1)
    for x in range(3,int(n**0.5)+1,2):
        for y in range(3,(n//x)+1,2):
            sieve[(x*y)]=False

    return [2]+[i for i in range(3,n,2) if sieve[i]]

def powerful(n):
	ps = primes(n)
	if len([a for a in ps if divisor(a, n)]) == 0:
		return False
	for p in ps:
		if not divisor(p**2, n) and divisor(p, n):
			return False
	return True

def perfect_power(n):
	if n == 1:
		return True
	if n < 3:
		return False
	for i in range(2, int(math.sqrt(n)) + 1):
		pow=1
		while i**pow < n:
			pow+=1
			if i**pow == n:
				return True
	return False

import fractions

def phi(n):
    amount = 0        
    for k in range(1, n + 1):
        if fractions.gcd(n, k) == 1:
            amount += 1
    return amount

def achiless(n):
	return powerful(n) and not perfect_power(n)

def strong_achiless(n):
	if not achiless(n):
		return False
	if not achiless(phi(n)):
		return False
	return True