// not solved yet

#include "../utils/defs.h"

ulli streak(ulli n)
{
    ulli k = 0;
    while(true)
    {
        if ((n+k) % (k+1) != 0)
            break;
        k++;
    }
    return k+1;
}

int main()
{
    ulli t, previ = 0;
    for (ulli i = 4; i < 2000; i++)
    {
        t = streak(i);
        if (t == 7)
        {
            cout << i << " " << i - previ << endl;
            previ = i;
        }
    }
}

// 2 - 2
// 3 - 2 4 2 4 2 4
// 4 - 12
// 5 - 12 12 12 24
// 6 - none
// 7 - 60 60 60 60 60 120