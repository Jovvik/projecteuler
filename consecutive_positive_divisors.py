def divisors(n):
    cnt = 1 #itself
    for i in range(1, n // 2 + 1):
        if n % i == 0:
            cnt += 1
    return cnt

dvs = []
for i in range(1, int(10e7)):
    dvs.append(divisors(i))

cnt = 0
for i in range(1, len(dvs) - 1):
    if dvs[i] == dvs[i+1]:
        cnt += 1

print(cnt)